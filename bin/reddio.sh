#!/bin/bash

SED=$(which gsed || which sed)
FMT=$(which gfmt || which fmt)

show_help(){
    echo "USAGE:"
    echo "      reddio.sh comments/<ID> [withlinks]"
    echo "      reddio.sh user/<ID>"
    exit 1
}

if ! [ $# -eq 1 ] && ( [ $# -eq 2 ] && ! echo $2 | grep -iq withlinks )
then
    show_help
fi

URL="$1"

if echo $URL | grep -iq '^http'
then
    IFS='/' read -r -a PARTS <<< "$URL"
    if echo ${PARTS[2]} | grep -iq 'reddit\.com'
    then
        if [ "${PARTS[5]}" = comments ]
        # normal link
        then
            URL="${PARTS[5]}/${PARTS[6]}"
        elif [ "${PARTS[3]}" = comments ]
        # flatten link (https://www.reddit.com/comments/abcdef)
        then
            URL="${PARTS[3]}/${PARTS[4]}"
        elif [ "${PARTS[5]}" = s ]
        # shared link
        then
            URL=$(curl -sLI "$URL" | awk -v FS='/' '/^location: /{print $6"/"$7; exit}')
        elif [ "${PARTS[3]}" = user ] || [ "${PARTS[3]}" = u ]
        # user link
        then
            URL="user/${PARTS[4]}"
        fi
    else
        echo "WARNING: Not a reddit URL provided"
        show_help
    fi
fi

if echo $URL | grep -iq '^u\/'
then
    URL=${URL/u\//user\/}
fi

if echo $@ | grep -iq withlinks
then
    WITHLINKS=$SED' s,t1_,https://www.reddit.com/'$URL'//,;s,t3_,https://redd.it/,'
else
    WITHLINKS=$SED' s,t3_,https://redd.it/,'
fi

if echo $URL | grep -iq '^comments\/' || echo $URL | grep -iq '^user\/'
then
    # highlight the OP's name with cyan, add 'u/'
    # remove the post's votes number
    # remove the votes numbers, highlight the users' names with puprple, add 'u/'
    # remove excessive coloring and user marks
    # (experimental) remove markdown code boundaries (backticks)
    # (experimental) replace markdown links with plain text ones
    # highlight links
    # remove comments by some crap bots: backtickbot, vim-help-bot, AutoModerator
    # remove other crap
    # set width
    reddio print -s old -c always $URL \
        | $SED 's/34m/36mu\//' \
        | $SED '1s/^\S\+\s/[37m/' \
        | $SED '/t1_/s/^\(\s*\)\S*37m \(\S*\)\(.*\)$/\1[35mu\/\2[0m\3/' \
        | $SED 's/\[35m\(u\/\)*\[36m/[36m/' \
        | $SED 's/`/ /g' \
        | $SED 's/\[\([^]]*\)\](\(http[^() ]\+\))/\1: \2/g' \
        | $SED -e 's/\b\(http.\+\): \1/\1/g' \
        | $SED 's/\(http[^() ]\+\)/\[38;5;067m\1\[0m/g' \
        | $SED '/\[35m\s*u\/vim-help-bot/,/\[35m/{/\[35m\s*u\/vim-help-bot/!{/\[35m/!d}};/\[35m\s*u\/vim-help-bot/d' \
        | $SED '/\[35m\s*u\/backtickbot/,/\[35m/{/\[35m\s*u\/backtickbot/!{/\[35m/!d}};/\[35m\s*u\/backtickbot/d' \
        | $SED '/\[35m\s*u\/RemindMeBot/,/\[35m/{/\[35m\s*u\/RemindMeBot/!{/\[35m/!d}};/\[35m\s*u\/RemindMeBot/d' \
        | $SED '/\[35m\s*u\/FatFingerHelperBot/,/\[35m/{/\[35m\s*u\/FatFingerHelperBot/!{/\[35m/!d}};/\[35m\s*u\/FatFingerHelperBot/d' \
        | $SED '/\[35m\s*u\/AutoModerator/,/\[35m/{/\[35m\s*u\/AutoModerator/!{/\[35m/!d}};/\[35m\s*u\/AutoModerator/d' \
        | $SED -E 's/&#x200B;|&#x200C;|&#x200D;|&#x200E;|&#x200F;|&#x99;|﻿|​|‌|‍|‎|‏//g' \
        | $WITHLINKS \
        | $FMT -sw$(($(tput cols)-10))
        # | $SED -z 's/\n/\n\n/g' | par w$(($(tput cols)-10)) d1 q1 s0 | $SED -z 's/\n\n/\n/g'
else
    show_help
fi
#         | tr -d $(printf '\xE2\x80\x8B') \
#         | tr -d $(printf '\xE2\x80\x8E') \
#         | tr -d ‍ \
#         | tr -d  \
