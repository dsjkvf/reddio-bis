reddio
======

## About

This is a fork of the CLI reddit client, [reddio](https://gitlab.com/aaronNG/reddio/), with some personal modifications applied, as well as with a simple wrapper script, `reddio.sh`, included.

All the credits, obviously, go to the original author, [Aaron Gießelmann](https://gitlab.com/aaronNG).
